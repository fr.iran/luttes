
.. index::
   ! Mahsa Jina Amini


.. _mahsa_jina_amini:

==========================================================================================================================================
**Mahsa Jina Amini** (née le 22 juin 2000 à Saqqez, assassinée le 16 septembre 2022 à Téhéran) #MasahAmini #0Piran #مهساامینی
==========================================================================================================================================


- https://fr.wikipedia.org/wiki/Mort_de_Mahsa_Amini

.. figure:: images/mahsa_amini_en_persan.png
   :align: center
   :width: 300

   Masah Amini, #مهساامینی


.. figure:: images/mahsa_amini_hopital.png
   :align: center
   :width: 600


.. figure:: images/tweet_sur_jina.png
   :align: center

   https://twitter.com/chowmak/status/1571229379606323208?s=20&t=THQuX7a2q36lRn3BIWWTJw


Manifestation de soutien en France
=====================================

- :ref:`iran_grenoble_2022_09_24`
