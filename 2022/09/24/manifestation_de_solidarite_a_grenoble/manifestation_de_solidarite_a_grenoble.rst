
.. index::
   pair: Mahsa Jina Amini; Grenoble
   pair: Iraniennes ; Ukrainiennes
   pair: Chant; Iranien


.. _iran_grenoble_2022_09_24:

===================================================================================================================================================================================================
Samedi 24 septembre 2022 à Grenoble place Félix Poulat à 14h30 **rassemblement de solidarité avec les femmes en Iran et en hommage à Mahsa Jina Amini, #Grenoble #MasahAmini #0Piran #مهساامینی**
===================================================================================================================================================================================================

- :ref:`mahsa_jina_amini`
- https://www.grenoble.fr/association/129060/69-ligue-des-droits-de-l-homme-et-du-citoyen.htm


Rassemblement de solidarité avec les femmes en Iran et en hommage à Mahsa Jina Amini
========================================================================================

Samedi 24 septembre 2022 place Félix Poulat à 14h30 rassemblement de
solidarité avec les femmes en Iran et en hommage à Mahsa Jina Amini



.. figure:: images/appel_a_solidarite.png
   :align: center
   :width: 300

.. figure:: images/appel_au_rassemblement.png
   :align: center
   :width: 300

Masah Amini, #مهساامینی
-----------------------

.. figure:: images/mahsa_amini_hopital.png
   :align: center
   :width: 600


.. figure:: images/mahsa_amini_en_persan.png
   :align: center
   :width: 300

   Masah Amini, #مهساامینی, :ref:`mahsa_jina_amini`


Manifestation à Grenoble, place Félix Poulat
---------------------------------------------

.. figure:: images//manifestants__et_tram.png
   :align: center


.. figure:: images/avec_les_ukrainiens_2.png
   :align: center

   Solidarité entre ukrainiens/ukrainiennes et iraniens/iraniennes



.. figure:: images/une_personne_avec_dessin_marjane_rose.png
   :align: center
   :width: 600


Mort à la république islamique, #MasahAmini
-----------------------------------------------

.. figure:: images/manifestantes__et_zoya_rose.png
   :align: center

   Mort à la république islamique, #MasahAmini

.. figure:: images/manifestantes__et_zoya_rose_2.png
   :align: center


Morte pour une mèche de cheveux en Iran
-------------------------------------------

.. figure:: images/morte_pour_une_meche_de_cheveux.png
   :align: center



Soyons avec les femmes iraniennes
------------------------------------

.. figure:: images/soyons_avec_les_femmes_iraniennes.png
   :align: center
   :width: 600



Femme (Zan, زن), Vie (Zendegi, زندگی), Liberté (Âzâdi, آزادی)
--------------------------------------------------------------

.. figure:: images/tissu_avec_3_mots.png
   :align: center
   :width: 600

   Femme (Zan, زن), Vie (Zendegi, زندگی), Liberté (Âzâdi, آزادی)



Femme, Vie, Liberté #MahsaAmini, #0Piran
------------------------------------------

.. figure:: images/femme_vie_liberte.png
   :align: center
   :width: 300

   Femme, Vie, Liberté #MahsaAmini, #0Piran



Morte pour ses cheveux en Iran, Femme (Zan, زن), Vie (Zendegi, زندگی), Liberté (Âzâdi, آزادی)
---------------------------------------------------------------------------------------------

.. figure:: images/femme_vie_liberte_plus_mahsa_rose.png
   :align: center

   Morte pour ses cheveux en Iran, Femme (Zan, زن), Vie (Zendegi, زندگی), Liberté (Âzâdi, آزادی)



.. figure:: images/sur_le_mur_4_images_mahsa.png
   :align: center


.. figure:: images/sur_le_mur_4_images_mahsa_dessin_marjane.png
   :align: center


.. figure:: images/sur_le_mur_8_images_plus_rose.png
   :align: center

.. figure:: images/manifestants__devant_eglise.png
   :align: center


.. figure:: images/sur_le_mur_9_images.png
   :align: center



Solidarité entre ukrainiens/ukrainiennes et iraniens/iraniennes
====================================================================

.. figure:: images/avec_les_ukrainiens.png
   :align: center

   Solidarité entre ukrainiens/ukrainiennes et iraniens/iraniennes


.. figure:: images/avec_les_ukrainiens_2.png
   :align: center

   Solidarité entre ukrainiens/ukrainiennes et iraniens/iraniennes


Chant iranien (que le son)
============================

**Le fait qu'il n'y a que le son est volontaire bien sûr**.

- https://www.youtube.com/watch?v=mu3FqmqIqac

